<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController {
    public static function findNumber($n) {
        $a = [0, 1];
        $i = 2;
        $max = 1;

        while ($i <= $n) {
            if ($i % 2 == 0) {
                $s = $i / 2;
                array_push($a, $a[$s]);
                $max = $a[$i] > $max ? $a[$i] : $max;
            } else {
                $s = ($i-1) / 2;
                array_push($a, $a[$s] + $a[$s + 1]);
                $max = $a[$i] > $max ? $a[$i] : $max;
            }
            $i++;
        }
        return $max;
    }

    /**
     * @Route("/")
     */
    public function index() {
        return $this->render('main/index.html.twig');
    }

    /**
     * @Route("/numbers")main
     * @param Request $request
     * @return Response
     */
    public function numbers(Request $request) {
        if ($request->getMethod() != 'POST') {
            return $this->render('error.html.twig');
        }

        $res = [];

        foreach ($request->request->get('numbers') as $value) {
            $val = $this->findNumber($value);
            $res[] = ["n" => $value, "res" => $val];
        }

        return $this->render('main/numbers.html.twig', ["var" => $res]);
    }
}
