<?php
namespace App\Command;

use App\Controller\MainController;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

require_once(__DIR__.'/../Controller/MainController.php');

class GetNumbersCommand extends Command {
    protected static $defaultName = 'app:find-numbers';

    private $numbers;

    public function __construct($numbers= []) {
        $this->numbers = $numbers;
        parent::__construct();
    }

    private function check($arr, $output) {
        $toSend = [];
        $toRemove = [];

        if (count($arr) > 10) {
            return 'toBig';
        }

        foreach ($arr as $a) {
            $v = (int)$a;
            $toSend[] = (int)$a;
            if (($v < 1 || $v > 99999)) {
                $output->writeln("Value ".$v." is to big, removed");
                $toRemove[] = $v;
            }
        }

        return array_diff($toSend, $toRemove);
    }

    protected function configure() {
        $this
            ->setDescription('Find max number in series from given number')
            ->setHelp('Finding numbers based on some series')
            ->addArgument('numbers', InputArgument::IS_ARRAY, 'Arrays of int');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $res = [];

        $arr = $this->check($input->getArgument('numbers'), $output);

        if ($arr === 'toBig') {
            $output->writeln('To many arguments!');
            return 0;
        }

        foreach ($arr as $value) {
            $val = MainController::findNumber($value);
            $res[] = ["n" => $value, "res" => $val];
        }

        $output->writeln("Your numbers:");
        foreach ($res as $value) {
            $output->writeln("For: ".$value['n']." max value is: ".$value['res']);
        }

        return 0;
    }
}