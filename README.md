# FIND MAX NUMBER IN "pregiven" SERIES
### Install dependencies

Just run this command in main directory

```sh
composer install
```

## FOR CONSOLE APPLICATION

```sh
php bin/console app:find-number <agrs>
```
For eg. `php bin/console app:find-number 5 10` should return
```sh
Your numbers:
For: 5 max value is: 3
For: 10 max values is: 4
```
## FOR WEB APPLICATION
If you want to run web server type:
```sh
symfony serve
```
